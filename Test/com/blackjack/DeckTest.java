package com.blackjack;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Stack;

class DeckTest {



    Stack<Card> deck = new Stack<>();
    Deck dealer = new Deck();

    @BeforeEach
    public void setUp(){
        dealer.init();
        deck = dealer.getDeck();
    }

    @Test
    void testPlayergetsAcardTest() {
        Player fred = new Player("Fred");
        dealer.dealACard(fred);
        Assertions.assertFalse(fred.getHand().isEmpty());
    }
    @Test
    void dealtCardIRemovedFromDeckTest() {
        Player fred = new Player("Fred");
        dealer.dealACard(fred);
        Card dealtCard = fred.getHand().get(0);
        Assertions.assertFalse(deck.contains(dealtCard));
    }

    @Test
    void dealAllPlayersTwoCards() {
        List<Player> playerList = List.of(new Player("fred"), new Player("keaku"), new Player("ri"));

        dealer.dealAllPlayersTwoCards(playerList);
        int total_cards_Dealt = playerList.stream()
                .map(Player::getHand)
                .map(hand->hand.size())
                .reduce(0, Integer::sum);
        Assertions.assertEquals(total_cards_Dealt, 6);

    }

    @Test
    void init() {
        dealer.init();
        Assertions.assertFalse(deck.isEmpty());

    }
}