package com.blackjack;

import java.text.CollationKey;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Blackjack {

    public static void main(String[] args) {


        Deck dealer = new Deck();
        final List<Player> players = List.of(new Player("Fred"), new Player("Kweku"), new Player("Ri"));
        boolean endOfGame = false;

        Predicate<List<Player>> gameStartCondition = startingPlayers -> (players.size() > 2 && players.size() <= 6);
        Predicate<List<Player>> playerHit21Condition = playersInGame -> {
            for (Player player : playersInGame) {
                if (player.getTotalCardValue() == 21) {
                    System.out.println(player.getName() + " has hit 21 ");
                    return true;
                }
            }
            return false;
        };
        Predicate<List<Player>> allPlayersStickCondition = playersInGame -> {
            long players_in_game = players.stream()
                    .map(Player::getStrategy)
                    .filter(strategy -> strategy != Strategy.GO_BUST)
                    .count();

            // All the players who have sticked
            long total_stick_players = players.stream()
                    .map(Player::getStrategy)
                    .filter(strategy -> strategy == Strategy.STICK)
                    .count();
            boolean result = total_stick_players == players_in_game;
            if (result) System.out.println("All players have STICK");
            return result;

        };
        Predicate<List<Player>> onePlayerLeftCondition = playersInGame ->{
            return false;
        };


        int i = 0;
        while (i>10){
            System.out.println(i);
            i++;
        }


        startGame(dealer, players, gameStartCondition);

      endOfGame =  checkEndOfGame(players, playerHit21Condition,allPlayersStickCondition, onePlayerLeftCondition );
      if(endOfGame) {
          declareWinner(players);
        } else  {

          while (!endOfGame){

              List<Player> playersInGame = players.stream()
                      .filter(player -> player.getStrategy() != Strategy.GO_BUST)
                      .collect(Collectors.toList());

              for (Player player: playersInGame){
                  dealer.dealACard(player);
                  player.updateStrategy();

                  endOfGame = checkEndOfGame(players, playerHit21Condition, allPlayersStickCondition, onePlayerLeftCondition);
                  if (endOfGame){
                      declareWinner(players);
                  }
              }




          }

      }


    }

    public static void declareWinner(List<Player> players) {
        int highestValue = 0;
        Player winner = null;
        players = players.stream()
                .filter(player -> player.getStrategy() != Strategy.GO_BUST)
                .collect(Collectors.toList());
        for (Player player: players){
            if (highestValue < player.getTotalCardValue()){
                highestValue = player.getTotalCardValue();
                winner = player;
            }
        }

        System.out.println("======================================================");
        System.out.println("GAME OVER!");
        System.out.println("Winner: \t" + winner.getName() +
                "\nScore: \t" + winner.getTotalCardValue());

        System.out.println("======================================================");
    }


    public static boolean playerHits21(List<Player> players, Predicate<List<Player>> winnerExistCondition) {
        return winnerExistCondition.test(players);
    }

    public static void startGame(Deck deck, List<Player> players, Predicate<List<Player>> gameStartCondition) {
        if (gameStartCondition.test(players)) {
            deck.init();
            deck.shuffleCards();
            System.out.println("Game starting with " + players.size() + " players");
            deck.dealAllPlayersTwoCards(players);
        } else {
            System.out.println("This Game can only be played by 3 or less than 6 players:");
        }


    }

    public static boolean allPlayersStick(List<Player> players, Predicate<List<Player>> allStickCondition) {
        return allStickCondition.test(players);
    }

    public static boolean checkEndOfGame
            (List<Player> players, Predicate<List<Player>> playerHits21Condition,
             Predicate<List<Player>> allPlayerStickCondition, Predicate<List<Player>> onePlayerLeftCondition
             ){
        return playerHits21Condition.test(players) || allPlayerStickCondition.test(players) || onePlayerLeftCondition.test(players);

    }
}