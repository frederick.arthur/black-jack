package com.blackjack;

public class Card {
    CardName cardName;
    Suit cardSuit;
    int value;

    public Card(CardName name, Suit cardSuit, int value) {
        this.cardName = name;
        this.cardSuit = cardSuit;
        this.value = value;
    }

    public CardName getCardName() {
        return cardName;
    }

    public Suit getCardSuit() {
        return cardSuit;
    }

    public int getValue() {
        return value;
    }
}
