package com.blackjack;

import java.util.*;

public class Deck {

    private final Stack<Card> deck = new Stack<>();

    public void dealACard(Player player) {
        Card card = deck.pop();
        player.getDeal(card);
        System.out.println(player.getName() + " dealt " + card.getCardName() + " of " + card.getCardSuit());

    }
public void dealACard(Player player, Strategy strategy) {
        Card card = deck.pop();
        player.getDeal(card);
        System.out.println(player.getName() + " dealt " + card.getCardName() + " of " + card.getCardSuit());

    }

    public void dealAllPlayersTwoCards(List<Player> players) {
        System.out.println("Dealing 2 cards to  all " + players.size() + " players");
        for (Player player : players) {
            Card card1 = deck.pop();
            Card card2 = deck.pop();
            player.getDeal(card1);
            player.getDeal(card2);
            player.getHand().forEach(card -> {
                System.out.println(card.cardName + " of " + card.cardSuit);
            });

        }
    }

    public void shuffleCards() {
        System.out.println("Shuffling cards in Deck...");
        Collections.shuffle(this.deck);
        System.out.println("Done shuffling cards in Deck!");
    }

    public void init() {
        System.out.println("Initializing the Deck...");
        Suit[] suits = {Suit.Clubs, Suit.Diamonds, Suit.Hearts, Suit.Spades};
        CardName[] names = {
                CardName.Two, CardName.Three, CardName.Four, CardName.Five,
                CardName.Six, CardName.Seven, CardName.Eight, CardName.Nine,
                CardName.Ten, CardName.Jack, CardName.King, CardName.Queen, CardName.Ace
        };

        for (Suit suit : suits) {
            for (int i = 0; i < names.length; i++) {
                CardName name = names[i];
                switch (name) {
                    case Ace:
                        this.deck.push(new Card(name, suit, 11));
                        break;
                    case Jack, King, Ten, Queen:
                        this.deck.push(new Card(name, suit, 10));
                        break;
                    default:
                        this.deck.push(new Card(name, suit, i + 2));
                }

            }
        }

        System.out.println("Deck initialization done...");
    }


    public Stack<Card> getDeck(){
        return this.deck;
    }

}
